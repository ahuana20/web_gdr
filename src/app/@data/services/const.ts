import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class Const {
  public static API_FILE_SERVER: string;
  public static API_SEGURIDAD: string;
  public static API_PERSONA: string;
  public static API_MAESTRA: string;
  public static API_ENTIDAD: string;
  public static API_CONVOCATORIA: string;
  public static API_EVALUACION: string;

  public static USERNAME_SEGURIDAD: string;
  public static PASSWORD_SEGURIDAD: string;
  public static APPLICATION_ID: number;

  public static R_ADMIN_SERVIR: number;
  public static R_ADMIN_ENTIDAD: number;
  public static R_GESTOR_ORH: number;
  public static R_COORDINADOR: number;
  public static R_CONFIGURADOR: number;

  public static MD_DL1041: string;
  public static MD_DL276: string;
  public static MD_DL728: string;
  public static MD_DL30057: string;
  public static MD_DL1057: string;

  public static M_NO_APLICA: string;
  public static T_NO_APLICA: string;

  public static MD_PRA_PRE_PROF: string;
  public static MD_PRA_PROF: string;

  public static EGRESADO: string;
  public static ESTUDIANTE: string;

  public static SIT_ACA_MAE: string;
  public static SIT_ACA_BAC: string;
  public static SIT_ACA_TIT_LIC: string;
  public static SIT_ACA_DOC: string;
  public static SIT_ACA_SEG_ESP: string;
  public static SIT_ACA_PRE: string;
  public static SIT_ACA_EGRE: string;

  public static DIPLOMADO: string;
  public static TALLER: string;
  public static CURSO: string;

  public static INF_BASE_LEGAL: string;
  public static INF_BONIFICACION: string;
  public static INF_DEC_JURADA: string;
  public static INF_CRI_EVALUACION: string;
  public static INF_BASE_LEGAL_ESP: string;

  public static ETA_BASE_PROCESO: string;
  public static ETA_BASE_APROBADO: string;
  public static ETA_BASE_COMPLETADO: string;
  public static ETA_BASE_OBSERVADO: string;
  public static ETA_BASE_ENVIADO: string;
  public static ETA_BASE_POR_REVISAR: string;
  public static ETA_BASE_REVISADO: string;
  public static ETA_BASE_PUBLICADO: string;
  public static ETA_BASE_POR_PUBLICAR: string;
  public static ETA_BASE_ELIMINADO: string;

  constructor(private http: HttpClient) {}

  public loadCommonConfig() {
    return this.http
      .get('./assets/config/common.config.json')
      .toPromise()
      .then((config: any) => {
        Const.API_FILE_SERVER = config.public_base_url_file_server;
        Const.API_SEGURIDAD = config.public_base_url_seguridad;
        Const.API_PERSONA = config.public_base_url_persona;
        Const.API_MAESTRA = config.public_base_url_maestra;
        Const.API_ENTIDAD = config.public_base_url_entidad;
        Const.API_CONVOCATORIA = config.public_base_url_convocatoria;
        Const.API_EVALUACION = config.public_base_url_evaluacion;
      })
      .catch((err: any) => {
        console.error(err);
      });
  }

  public loadEntidadConfig() {
    return this.http
      .get('./assets/config/sgr-web.config.json')
      .toPromise()
      .then((config: any) => {
        Const.USERNAME_SEGURIDAD = config.client_id;
        Const.PASSWORD_SEGURIDAD = config.client_secret;
        Const.APPLICATION_ID = config.aplicacion_id;

        Const.R_ADMIN_SERVIR = config.roles.admin_servir;
        Const.R_ADMIN_ENTIDAD = config.roles.admin_entidad;
        Const.R_GESTOR_ORH = config.roles.gestor_orh;
        Const.R_COORDINADOR = config.roles.coordinador;
        Const.R_CONFIGURADOR = config.roles.configurador;

        Const.MD_DL30057 = config.maestraDetalle.ley30057;
        Const.MD_DL1041 = config.maestraDetalle.dl1401;
        Const.MD_DL276 = config.maestraDetalle.dl276;
        Const.MD_DL728 = config.maestraDetalle.dl728;
        Const.MD_DL1057 = config.maestraDetalle.dl1057;

        Const.M_NO_APLICA = config.maestraDetalle.mod_no_aplica;
        Const.T_NO_APLICA = config.maestraDetalle.tipo_no_aplica;

        Const.EGRESADO = config.maestraDetalle.egresado;
        Const.ESTUDIANTE = config.maestraDetalle.estudiante;

        Const.MD_PRA_PRE_PROF = config.maestraDetalle.pract_preprofesional;
        Const.MD_PRA_PROF = config.maestraDetalle.pract_profesional;

        Const.SIT_ACA_MAE = config.maestraDetalle.sit_acad_maestria;
        Const.SIT_ACA_BAC = config.maestraDetalle.sit_acad_bachiller;
        Const.SIT_ACA_TIT_LIC = config.maestraDetalle.sit_acad_titulo_lic;
        Const.SIT_ACA_DOC = config.maestraDetalle.sit_acad_doctorado;
        Const.SIT_ACA_SEG_ESP = config.maestraDetalle.sit_acad_segunda_esp;
        Const.SIT_ACA_PRE = config.maestraDetalle.sit_acad_pregrado;
        Const.SIT_ACA_EGRE = config.maestraDetalle.sit_acad_egresado;

        Const.DIPLOMADO = config.maestraDetalle.diplomado;
        Const.TALLER = config.maestraDetalle.taller;
        Const.CURSO = config.maestraDetalle.curso;

        Const.INF_BASE_LEGAL = config.informes.base_legal;
        Const.INF_BONIFICACION = config.informes.bonificaciones;
        Const.INF_DEC_JURADA = config.informes.declara_jurada;
        Const.INF_CRI_EVALUACION = config.informes.crit_evaluacion;
        Const.INF_BASE_LEGAL_ESP = config.informes.base_legal_esp;

        Const.ETA_BASE_PROCESO = config.etapa_base.proceso;
        Const.ETA_BASE_APROBADO = config.etapa_base.aprobado;
        Const.ETA_BASE_COMPLETADO = config.etapa_base.completado;
        Const.ETA_BASE_OBSERVADO = config.etapa_base.observado;
        Const.ETA_BASE_ENVIADO = config.etapa_base.enviado;

        Const.ETA_BASE_POR_REVISAR = config.etapa_base.por_revisar;
        Const.ETA_BASE_REVISADO = config.etapa_base.revisado;
        Const.ETA_BASE_PUBLICADO = config.etapa_base.publicado;
        Const.ETA_BASE_POR_PUBLICAR = config.etapa_base.por_publicar;
        Const.ETA_BASE_ELIMINADO = config.etapa_base.eliminado;
      })
      .catch((err: any) => {
        console.error(err);
      });
  }
}
