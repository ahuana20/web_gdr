import {
  ModuleWithProviders,
  NgModule,
  Optional,
  SkipSelf,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AdministratorService,
  AuthenticationService,
  ParameterService,
  SunatService,
} from '../@data/services';
import { throwIfAlreadyLoaded } from './module-import-guard';
import { AuthenticationRepository } from './repository/authentication.repository';
import { ParameterRepository } from './repository/parameter.repository';
import { SunatRepository } from './repository/sunat.repository';
import { AdministratorRepository } from './repository/administrator.repository';

import { ReniecRepository } from './repository/reniec.repository';
import { ReniecService } from '../@data/services/reniec.service';

import { PerfilesRepository } from './repository/perfiles.repository';
import { PerfilesService } from 'src/app/@data/services/perfiles.service';

import { BannerRepository } from './repository/banner.repository';
import { BannerService } from '../@data/services/banner.service';

const DATA_SERVICES = [
  {
    provide: AuthenticationRepository,
    useClass: AuthenticationService,
  },
  { provide: ParameterRepository, useClass: ParameterService },
  { provide: SunatRepository, useClass: SunatService },

  { provide: AdministratorRepository, useClass: AdministratorService },
  { provide: ReniecRepository, useClass: ReniecService },

  { provide: PerfilesRepository, useClass: PerfilesService },

  { provide: BannerRepository, useClass: BannerService }
];

@NgModule({
  declarations: [],
  imports: [CommonModule],
})
export class DomainModule {
  constructor(@Optional() @SkipSelf() parentModule: DomainModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }

  static forRoot(): ModuleWithProviders<any> {
    return {
      ngModule: DomainModule,
      providers: [...DATA_SERVICES],
    } as ModuleWithProviders<any>;
  }
}
